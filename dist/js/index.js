/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ([
/* 0 */,
/* 1 */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Bot)
/* harmony export */ });
/* harmony import */ var _RegistryObject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _Command__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);
/* harmony import */ var _PermissionGroup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4);
/* harmony import */ var _Database__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6);




class Prefix extends _RegistryObject__WEBPACK_IMPORTED_MODULE_0__.RegistryObject {
    constructor(_id, acc) {
        super();
        this._id = `prefix.${_id}`;
        this.accessor = acc;
        Bot.register(this._id, this);
    }
}
class Bot {
    static start(client) {
        this.client = client;
        this.registry = new Map();
        this.bindDefaultPermissionGroups();
        this.bindPrefixes();
        this.bindDefaultCommands();
        this.bindEventListeners();
        if (_Database__WEBPACK_IMPORTED_MODULE_3__.Database.getUser(this.client.getOwnParticipant()._id) == null) {
            let user = _Database__WEBPACK_IMPORTED_MODULE_3__.Database.createUser(this.client.getOwnParticipant());
            user.groups.push("owner");
            _Database__WEBPACK_IMPORTED_MODULE_3__.Database.updateUser(this.client.getOwnParticipant()._id, user);
        }
        return this;
    }
    static bindDefaultPermissionGroups() {
        new _PermissionGroup__WEBPACK_IMPORTED_MODULE_2__.PermissionGroup("default").addPermission("command.*");
        new _PermissionGroup__WEBPACK_IMPORTED_MODULE_2__.PermissionGroup("admin").addPermission("admincommand.*");
        new _PermissionGroup__WEBPACK_IMPORTED_MODULE_2__.PermissionGroup("special").addPermission("special.*");
        new _PermissionGroup__WEBPACK_IMPORTED_MODULE_2__.PermissionGroup("owner").addPermission("*");
    }
    static bindDefaultCommands() {
        new _Command__WEBPACK_IMPORTED_MODULE_1__.Command("help", ["help", "cmds", "h"], `$PREFIXhelp (command)`, `List commands.`, (msg, bot) => {
            let out = "";
            if (!msg.args[1]) {
                out = "Commands: ";
                for (let i of this.registry.keys()) {
                    if (!i.startsWith('command'))
                        continue;
                    let cmd = this.registry.get(i);
                    if (!cmd.hidden) {
                        let hasPermission = false;
                        for (let g of msg.user.groups) {
                            if (_PermissionGroup__WEBPACK_IMPORTED_MODULE_2__.PermissionGroup.hasPermission(g, cmd.permissionNode)) {
                                hasPermission = true;
                                break;
                            }
                        }
                        if (hasPermission) {
                            out += ` ${msg.usedPrefix.accessor}${cmd.accessors[0]} | `;
                        }
                    }
                }
                out = out.substr(0, out.length - 2).trim();
            }
            else {
                out = `There is no help for '${msg.argcat}'.`;
                for (let i of this.registry) {
                    if (!i[0].startsWith('command'))
                        continue;
                    let cmd = i[1];
                    let cont = false;
                    for (let a of cmd.accessors) {
                        if (msg.argcat == a) {
                            cont = true;
                            break;
                        }
                    }
                    if (!cont)
                        continue;
                    out = `${cmd.desc} Usage: ${_Command__WEBPACK_IMPORTED_MODULE_1__.Command.getUsage(cmd.usage, msg.usedPrefix.accessor)}`;
                }
            }
            return out;
        }, 0, "command.help", false);
        new _Command__WEBPACK_IMPORTED_MODULE_1__.Command("about", ["about"], `$PREFIXabout`, `About the bot.`, (msg, bot) => {
            return `Written in TypeScript and compiled with webpack by Hri7566.`;
        }, 0, "command.about", false);
        // admin commands
        new _Command__WEBPACK_IMPORTED_MODULE_1__.AdminCommand("permission", ["permission", "perm", "permissions", "perms", "luckperms", "lp"], `$PREFIXpermission [group|user] (create) [groupId [set|check] [permission] [value]|userId [addgroup] [groupId]]`, `Change permissions of groups or add users to groups`, (msg, bot) => {
            if (msg.args[1] == 'group') {
                if (!msg.args[2])
                    return `Usage: group (create) [groupId] [set|check] [permission] [value]`;
                if (msg.args[2] == 'create') {
                    if (!msg.args[3])
                        return `Provide a group name.`;
                    new _PermissionGroup__WEBPACK_IMPORTED_MODULE_2__.PermissionGroup(msg.args[3]);
                    return `Created group '${msg.args[3]}'.`;
                }
                else {
                    let group = this.registry.get(`permissiongroup.${msg.args[2]}`);
                    if (!group)
                        return `No group named '${msg.args[2]}' was found.`;
                    if (!msg.args[3])
                        return `Usage: group [groupId] [set|check] [permission] [value]`;
                    if (msg.args[3] == "set") {
                        if (!msg.args[4] && (!msg.args[5] || (msg.args[5] !== 'true' && msg.args[5] !== 'false')))
                            return `Usage: group [groupId] [set] [permission] [true|false]`;
                        if (msg.args[5] == "true") {
                            group.addPermission(msg.args[4]);
                        }
                        else if (msg.args[5] == "false") {
                            group.removePermission(msg.args[4]);
                        }
                        else {
                            return `Usage: group [groupId] [set] [permission] [true|false]`;
                        }
                    }
                    else if (msg.args[3] == "check") {
                        if (!msg.args[4])
                            return `Usage: group [groupId] [check] [permission]`;
                        let has = group.permissions.indexOf(msg.args[4]) !== -1;
                        return has ? `Group ${group._id} has ${msg.args[4]}` : `Group ${group._id} does not have ${msg.args[4]}`;
                    }
                    else {
                        return `Usage: group [groupId] [set|check] [permission] [value]`;
                    }
                }
            }
            else if (msg.args[1] == 'user') {
                if (!msg.args[2] || !msg.args[3])
                    return `Usage: user [userId] [addgroup] [groupId]`;
                let user = _Database__WEBPACK_IMPORTED_MODULE_3__.Database.getUser(msg.args[2]);
                if (!user)
                    return `No user with the _id '${msg.args[2]}' was found.`;
                if (msg.args[3] == "addgroup") {
                    if (!msg.args[4])
                        return `Usage: user [userId] addgroup [groupId]`;
                    let group = this.registry.get(`permissiongroup.${msg.args[4]}`);
                    if (!group)
                        return `No group named '${msg.args[4]}' was found.`;
                    user.groups.push(msg.args[4]);
                    _Database__WEBPACK_IMPORTED_MODULE_3__.Database.updateUser(user._id, user);
                    return `Added group '${msg.args[4]}' to user '${user._id}'.`;
                }
                else {
                    return `Usage: user [userId] addgroup [groupId]`;
                }
            }
        }, 1, `admincommand.permission`, false);
        new _Command__WEBPACK_IMPORTED_MODULE_1__.Command("permissiongroup", ["permissiongroup", "pg", "rank", "r"], `$PREFIXpermissiongroup`, `Check what groups you are in.`, (msg, bot) => {
            return `Groups: ${msg.user.groups.join(", ")}`;
        }, 0, `command.permissiongroup`, false);
    }
    static bindEventListeners() {
        if (typeof globalThis.require !== "undefined") {
            this.client.on('hi', (msg) => {
                this.client.sendArray([{
                        m: "userset",
                        set: {
                            name: "7566",
                            color: "#8d3f50"
                        }
                    }]);
            });
        }
        this.client.on('a', (msg) => {
            msg.args = msg.a.split(' ');
            for (let i of this.registry.keys()) {
                if (!i.startsWith('prefix'))
                    continue;
                let prefix = this.registry.get(i);
                if (msg.args[0].startsWith(prefix.accessor)) {
                    msg.usedPrefix = prefix;
                    break;
                }
            }
            if (!msg.usedPrefix)
                return;
            msg.cmd = msg.args[0].substring(msg.usedPrefix.accessor.length);
            msg.argcat = msg.a.substring(msg.args[0].length).trim();
            msg.user = _Database__WEBPACK_IMPORTED_MODULE_3__.Database.getUser(msg.p._id);
            if (!msg.user)
                msg.user = _Database__WEBPACK_IMPORTED_MODULE_3__.Database.createUser(msg);
            this.handleCommandMessage(msg);
        });
    }
    static handleCommandMessage(msg) {
        for (let i of this.registry) {
            if (!i[0].startsWith('command'))
                continue;
            let cmd = this.registry.get(i[0]);
            let canContinue = false;
            for (let a of cmd.accessors) {
                if (msg.cmd == a) {
                    canContinue = true;
                    break;
                }
            }
            if (canContinue == false)
                continue;
            let hasPermission = false;
            for (let g of msg.user.groups) {
                if (_PermissionGroup__WEBPACK_IMPORTED_MODULE_2__.PermissionGroup.hasPermission(g, cmd.permissionNode)) {
                    hasPermission = true;
                    break;
                }
            }
            if (hasPermission == false) {
                this.sendChat(`You do not have permission to use this command.`);
                break;
            }
            if (msg.args.length - 1 < cmd.minimumArguments) {
                this.sendChat(`Not enough arguments. Usage: ${_Command__WEBPACK_IMPORTED_MODULE_1__.Command.getUsage(cmd.usage, msg.usedPrefix.accessor)}`);
                break;
            }
            try {
                let out = cmd.func(msg, this);
                if (typeof (out) !== 'string')
                    break;
                if (out !== null && out !== '') {
                    this.sendChat(out);
                }
            }
            catch (err) {
                this.sendChat(`An error has occurred.`);
                console.error(err);
            }
        }
    }
    static bindPrefixes() {
        new Prefix("slash", "/");
        new Prefix("exclamation", "!");
    }
    static register(node, obj) {
        this.registry.set(node, obj);
    }
    static sendChat(str) {
        this.client.sendArray([{ m: "a", message: `\u034f${str}` }]);
    }
    static [Symbol.toPrimitive]() {
        return "🤡";
    }
    static [Symbol.toStringTag]() {
        return `🤡`;
    }
}
Bot.Database = _Database__WEBPACK_IMPORTED_MODULE_3__.Database;


/***/ }),
/* 2 */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegistryObject": () => (/* binding */ RegistryObject)
/* harmony export */ });
class RegistryObject {
}



/***/ }),
/* 3 */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Command": () => (/* binding */ Command),
/* harmony export */   "AdminCommand": () => (/* binding */ AdminCommand)
/* harmony export */ });
/* harmony import */ var _Bot__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var _RegistryObject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);


class Command extends _RegistryObject__WEBPACK_IMPORTED_MODULE_1__.RegistryObject {
    constructor(_id, accessors, usage, desc, func, minargs, node, hidden) {
        super();
        this._id = "command." + _id;
        this.accessors = accessors;
        this.func = func;
        if (!this.func)
            this.func = () => { return `This command has no function.`; };
        this.addPermission(node);
        this.hidden = hidden;
        this.desc = desc || "No description.";
        this.usage = usage || "No usage.";
        _Bot__WEBPACK_IMPORTED_MODULE_0__.default.register(this._id, this);
    }
    addPermission(node) {
        this.permissionNode = node;
    }
    static getUsage(usage, prefixAccessor) {
        return `${usage.split("$PREFIX").join(prefixAccessor)}`;
    }
}
class AdminCommand extends Command {
    constructor(_id, accessors, usage, desc, func, minargs, node, hidden) {
        super(_id, accessors, usage, desc, func, minargs, `admin.${node}`, hidden);
    }
    addPermission(node) {
        this.permissionNode = "admincommand." + node;
    }
}



/***/ }),
/* 4 */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PermissionGroup": () => (/* binding */ PermissionGroup)
/* harmony export */ });
/* harmony import */ var _RegistryObject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _Bot__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var _Permissions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5);



class PermissionGroup extends _RegistryObject__WEBPACK_IMPORTED_MODULE_0__.RegistryObject {
    constructor(_id) {
        super();
        this._id = "permissiongroup." + _id;
        this.permissions = [];
        _Bot__WEBPACK_IMPORTED_MODULE_1__.default.register(this._id, this);
    }
    addPermission(key) {
        return this.permissions.push(key);
    }
    removePermission(key) {
        return this.permissions.splice(this.permissions.indexOf(key));
    }
    static hasPermission(g, key) {
        let group = _Bot__WEBPACK_IMPORTED_MODULE_1__.default.registry.get("permissiongroup." + g);
        let out = false;
        for (let i of group.permissions) {
            if (_Permissions__WEBPACK_IMPORTED_MODULE_2__.Permissions.hasPermission(i, key)) {
                out = true;
                break;
            }
        }
        return out;
    }
    static getGroups(user) {
        let out = [];
        for (let g of user.groups) {
            if (_Bot__WEBPACK_IMPORTED_MODULE_1__.default.registry.has("permissiongroup." + g)) {
                out.push(_Bot__WEBPACK_IMPORTED_MODULE_1__.default.registry.get("permissiongroup." + g));
            }
        }
        if (out.length == 0) {
            out = [_Bot__WEBPACK_IMPORTED_MODULE_1__.default.registry.get("permissiongroup.default")];
        }
        return out;
    }
}



/***/ }),
/* 5 */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Permissions": () => (/* binding */ Permissions)
/* harmony export */ });
class Permissions {
    /**
     * Checks if a permission string is included within another.
     * @param perm1 The permission that needs to be checked
     * @param perm2 The permission to check against
     */
    static hasPermission(perm1, perm2) {
        let p1 = perm1.split('.');
        let p2 = perm2.split('.');
        let good = false;
        for (let i in p1) {
            if (p1[i] == p2[i] || p1[i] == '*') {
                good = true;
                continue;
            }
            else {
                good = false;
                break;
            }
        }
        return good;
    }
}



/***/ }),
/* 6 */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Database": () => (/* binding */ Database)
/* harmony export */ });
// node only
// if (typeof (globalThis as any).localStorage == 'undefined') {
//     let level = (require as any)('level');
//     let db = level("./bot.db");
//     (globalThis as any).localStorage = {
//         getItem: async (key: string) => {
//             return await db.get(key);
//         },
//         setItem: async (key: string, value: string) => {
//             return await db.put(key, value);
//         }
//     }
// }
class Database {
    static getUser(_id) {
        let data = localStorage.getItem("ts~" + _id);
        return JSON.parse(data);
    }
    static updateUser(_id, data) {
        let user = this.getUser(_id);
        for (let key of Object.keys(data)) {
            user[key] = data[key];
        }
        localStorage.setItem("ts~" + _id, JSON.stringify(data));
    }
    static createUser(data) {
        let user = data;
        user.groups = ["default"];
        localStorage.setItem("ts~" + user._id, JSON.stringify(data));
        return user;
    }
}



/***/ }),
/* 7 */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Client": () => (/* reexport safe */ _Client__WEBPACK_IMPORTED_MODULE_0__.Client),
/* harmony export */   "NoteQuota": () => (/* reexport safe */ _NoteQuota__WEBPACK_IMPORTED_MODULE_1__.NoteQuota)
/* harmony export */ });
/* harmony import */ var _Client__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8);
/* harmony import */ var _NoteQuota__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(10);





/***/ }),
/* 8 */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Client": () => (/* binding */ Client)
/* harmony export */ });
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9);
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(events__WEBPACK_IMPORTED_MODULE_0__);

class Client extends events__WEBPACK_IMPORTED_MODULE_0__.EventEmitter {
    constructor(uri) {
        super();
        this.offlineChannelSettings = { color: "#ecfaed" };
        this.offlineParticipant = { _id: "", name: "", color: "#777" };
        this.uri = uri;
        this.ws = undefined;
        this.serverTimeOffset = 0;
        this.user = undefined;
        this.participantId = undefined;
        this.channel = undefined;
        this.ppl = {};
        this.connectionTime = undefined;
        this.connectionAttempts = 0;
        this.desiredChannelId = undefined;
        this.desiredChannelSettings = undefined;
        this.pingInterval = undefined;
        this.canConnect = false;
        this.noteBuffer = [];
        this.noteBufferTime = 0;
        this.noteFlushInterval = undefined;
        this['🐈'] = 0;
        this.offlineParticipant = {
            _id: "",
            name: "",
            color: "#777",
            id: ""
        };
        this.autoPickupCrown = true;
        this.bindEventListeners();
    }
    isSupported() {
        return typeof WebSocket === "function";
    }
    isConnected() {
        return this.isSupported() && this.ws !== undefined && this.ws.readyState === WebSocket.OPEN;
    }
    isConnecting() {
        return this.isSupported() && this.ws !== undefined && this.ws.readyState === WebSocket.CONNECTING;
    }
    start() {
        this.canConnect = true;
        this.connect();
    }
    stop() {
        this.canConnect = false;
        this.ws.close();
    }
    connect() {
        if (!this.canConnect || !this.isSupported() || this.isConnected() || this.isConnecting())
            return;
        this.emit("status", "Connecting...");
        this.ws = new WebSocket(this.uri);
        let self = this;
        this.ws.addEventListener("close", function (evt) {
            self.user = undefined;
            self.participantId = undefined;
            self.channel = undefined;
            self.setParticipants([]);
            clearInterval(self.pingInterval);
            clearInterval(self.noteFlushInterval);
            self.emit("disconnect", evt);
            self.emit("status", "Offline mode");
            // reconnect!
            if (self.connectionTime) {
                self.connectionTime = undefined;
                self.connectionAttempts = 0;
            }
            else {
                ++self.connectionAttempts;
            }
            let ms_lut = [50, 2950, 7000, 10000];
            let idx = self.connectionAttempts;
            if (idx >= ms_lut.length)
                idx = ms_lut.length - 1;
            let ms = ms_lut[idx];
            setTimeout(self.connect.bind(self), ms);
        });
        this.ws.addEventListener("error", function (err) {
            self.emit("wserror", err);
            self.ws.close(); // self.ws.emit("close");
        });
        this.ws.addEventListener("open", function (evt) {
            self.connectionTime = Date.now();
            self.sendArray([{ "m": "hi", "_id": "tsbot" }]);
            self.pingInterval = globalThis.setInterval(function () {
                self.sendArray([{ m: "t", e: Date.now() }]);
            }, 20000);
            //self.sendArray([{m: "t", e: Date.now()}]);
            self.noteBuffer = [];
            self.noteBufferTime = 0;
            self.noteFlushInterval = globalThis.setInterval(function () {
                if (self.noteBufferTime && self.noteBuffer.length > 0) {
                    self.sendArray([{ m: "n", t: self.noteBufferTime + self.serverTimeOffset, n: self.noteBuffer }]);
                    self.noteBufferTime = 0;
                    self.noteBuffer = [];
                }
            }, 200);
            self.emit("connect");
            self.emit("status", "Joining channel...");
        });
        this.ws.addEventListener("message", function (evt) {
            let transmission = JSON.parse(evt.data);
            for (let i = 0; i < transmission.length; i++) {
                let msg = transmission[i];
                self.emit(msg.m, msg);
            }
        });
    }
    bindEventListeners() {
        this.on("hi", msg => {
            this.user = msg.u;
            this.receiveServerTime(msg.t, undefined);
            if (this.desiredChannelId) {
                this.setChannel();
            }
        });
        this.on("t", msg => {
            this.receiveServerTime(msg.t, msg.e || undefined);
        });
        this.on("ch", msg => {
            this.desiredChannelId = msg.ch._id;
            this.desiredChannelSettings = msg.ch.settings;
            this.channel = msg.ch;
            if (msg.p)
                this.participantId = msg.p;
            this.setParticipants(msg.ppl);
        });
        this.on("p", msg => {
            this.participantUpdate(msg);
            this.emit("participant update", this.findParticipantById(msg.id));
        });
        this.on("m", msg => {
            if (this.ppl.hasOwnProperty(msg.id)) {
                this.participantUpdate(msg);
            }
        });
        this.on("bye", msg => {
            this.removeParticipant(msg.p);
        });
        this.on("ch", msg => {
            if (this.autoPickupCrown) {
                if (msg.ch.crown) {
                    var crown = msg.ch.crown;
                    if (!crown.participantId || !this.ppl[crown.participantId]) {
                        let land_time = crown.time + 2000 - this.serverTimeOffset;
                        let avail_time = crown.time + 15000 - this.serverTimeOffset;
                        let countdown_interval = setInterval(function () {
                            let time = Date.now();
                            if (time >= land_time) {
                                let ms = avail_time - time;
                                if (ms <= 0) {
                                    clearInterval(countdown_interval);
                                    this.pickupCrown();
                                }
                            }
                        }, 1000);
                    }
                }
            }
        });
    }
    send(raw) {
        if (this.isConnected())
            this.ws.send(raw);
    }
    sendArray(arr) {
        this.send(JSON.stringify(arr));
    }
    setChannel(id, set) {
        this.desiredChannelId = id || this.desiredChannelId || "lobby";
        this.desiredChannelSettings = set || this.desiredChannelSettings || undefined;
        this.sendArray([{ m: "ch", _id: this.desiredChannelId, set: this.desiredChannelSettings }]);
    }
    getChannelSetting(key) {
        if (!this.isConnected() || !this.channel || !this.channel.settings) {
            return this.offlineChannelSettings[key];
        }
        return this.channel.settings[key];
    }
    setChannelSettings(settings) {
        if (!this.isConnected() || !this.channel || !this.channel.settings) {
            return;
        }
        if (this.desiredChannelSettings) {
            for (let key in settings) {
                this.desiredChannelSettings[key] = settings[key];
            }
            this.sendArray([{ m: "chset", set: this.desiredChannelSettings }]);
        }
    }
    getOwnParticipant() {
        return this.findParticipantById(this.participantId);
    }
    setParticipants(ppl) {
        for (let id in this.ppl) {
            if (!this.ppl.hasOwnProperty(id))
                continue;
            let found = false;
            for (let j = 0; j < ppl.length; j++) {
                if (ppl[j].id === id) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                this.removeParticipant(id);
            }
        }
        // update all
        for (let i = 0; i < ppl.length; i++) {
            this.participantUpdate(ppl[i]);
        }
    }
    countParticipants() {
        let count = 0;
        for (let i in this.ppl) {
            if (this.ppl.hasOwnProperty(i))
                ++count;
        }
        return count;
    }
    participantUpdate(update) {
        let part = this.ppl[update.id] || null;
        if (part === null) {
            part = update;
            this.ppl[part.id] = part;
            this.emit("participant added", part);
            this.emit("count", this.countParticipants());
        }
        else {
            if (update.x)
                part.x = update.x;
            if (update.y)
                part.y = update.y;
            if (update.color)
                part.color = update.color;
            if (update.name)
                part.name = update.name;
        }
    }
    removeParticipant(id) {
        if (this.ppl.hasOwnProperty(id)) {
            let part = this.ppl[id];
            delete this.ppl[id];
            this.emit("participant removed", part);
            this.emit("count", this.countParticipants());
        }
    }
    findParticipantById(id) {
        return this.ppl[id] || this.offlineParticipant;
    }
    isOwner() {
        return this.channel && this.channel.crown && this.channel.crown.participantId === this.participantId;
    }
    preventsPlaying() {
        return this.isConnected() && !this.isOwner() && this.getChannelSetting("crownsolo") === true;
    }
    receiveServerTime(time, echo) {
        let now = Date.now();
        let target = time - now;
        //console.log("Target serverTimeOffset: " + target);
        let duration = 1000;
        let step = 0;
        let steps = 50;
        let step_ms = duration / steps;
        let difference = target - this.serverTimeOffset;
        let inc = difference / steps;
        let iv = setInterval(() => {
            this.serverTimeOffset += inc;
            if (++step >= steps) {
                clearInterval(iv);
                //console.log("serverTimeOffset reached: " + self.serverTimeOffset);
                this.serverTimeOffset = target;
            }
        }, step_ms);
        // smoothen
        //this.serverTimeOffset = time - now;			// mostly time zone offset ... also the lags so todo smoothen this
        // not smooth:
        //if(echo) this.serverTimeOffset += echo - now;	// mostly round trip time offset
    }
    startNote(note, vel) {
        if (this.isConnected()) {
            vel = typeof vel === "undefined" ? undefined : +vel.toFixed(3);
            if (!this.noteBufferTime) {
                this.noteBufferTime = Date.now();
                this.noteBuffer.push({ n: note, v: vel });
            }
            else {
                this.noteBuffer.push({ d: Date.now() - this.noteBufferTime, n: note, v: vel });
            }
        }
    }
    ;
    stopNote(note) {
        if (this.isConnected()) {
            if (!this.noteBufferTime) {
                this.noteBufferTime = Date.now();
                this.noteBuffer.push({ n: note, s: 1 });
            }
            else {
                this.noteBuffer.push({ d: Date.now() - this.noteBufferTime, n: note, s: 1 });
            }
        }
    }
    ;
    // Where do these methods come from???
    setName(str) {
        if (str.length > 40)
            return;
        this.sendArray([{ m: 'userset', set: { name: str } }]);
    }
    kickban(_id, ms) {
        if (ms > 60 * 60 * 1000)
            ms = 60 * 60 * 1000;
        if (ms < 0)
            ms = 0;
        this.sendArray([{ m: 'kickban', _id: _id, ms: ms }]);
    }
    chown(id) {
        if (!this.isOwner())
            return;
        this.sendArray([{ m: 'chown', id: id }]);
    }
    pickupCrown() {
        this.sendArray([{ m: 'chown', id: this.getOwnParticipant().id }]);
    }
    getParticipant(str) {
        let ret;
        for (let id in this.ppl) {
            let part = this.ppl[id];
            if (part.name.toLowerCase().includes(str.toLowerCase()) || part._id.toLowerCase().includes(str.toLowerCase()) || part.id.toLowerCase().includes(str.toLowerCase())) {
                ret = part;
            }
        }
        if (typeof ret !== "undefined") {
            return ret;
        }
    }
}


/***/ }),
/* 9 */
/***/ ((module) => {

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;
module.exports.once = once;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

function checkListener(listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
}

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function _getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  checkListener(listener);

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0)
      return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      checkListener(listener);
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      checkListener(listener);

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}

function once(emitter, name) {
  return new Promise(function (resolve, reject) {
    function errorListener(err) {
      emitter.removeListener(name, resolver);
      reject(err);
    }

    function resolver() {
      if (typeof emitter.removeListener === 'function') {
        emitter.removeListener('error', errorListener);
      }
      resolve([].slice.call(arguments));
    };

    eventTargetAgnosticAddListener(emitter, name, resolver, { once: true });
    if (name !== 'error') {
      addErrorHandlerIfEventEmitter(emitter, errorListener, { once: true });
    }
  });
}

function addErrorHandlerIfEventEmitter(emitter, handler, flags) {
  if (typeof emitter.on === 'function') {
    eventTargetAgnosticAddListener(emitter, 'error', handler, flags);
  }
}

function eventTargetAgnosticAddListener(emitter, name, listener, flags) {
  if (typeof emitter.on === 'function') {
    if (flags.once) {
      emitter.once(name, listener);
    } else {
      emitter.on(name, listener);
    }
  } else if (typeof emitter.addEventListener === 'function') {
    // EventTarget does not have `error` event semantics like Node
    // EventEmitters, we do not listen for `error` events here.
    emitter.addEventListener(name, function wrapListener(arg) {
      // IE does not have builtin `{ once: true }` support so we
      // have to do it manually.
      if (flags.once) {
        emitter.removeEventListener(name, wrapListener);
      }
      listener(arg);
    });
  } else {
    throw new TypeError('The "emitter" argument must be of type EventEmitter. Received type ' + typeof emitter);
  }
}


/***/ }),
/* 10 */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NoteQuota": () => (/* binding */ NoteQuota)
/* harmony export */ });
class NoteQuota {
    constructor(cb) {
        this.cb = cb;
        this.setParams();
        this.resetPoints();
    }
    setParams(params = NoteQuota.PARAMS_OFFLINE) {
        // FIXME: Are these values ever falsy?
        let allowance = params.allowance || this.allowance || NoteQuota.PARAMS_OFFLINE.allowance;
        let max = params.max || this.max || NoteQuota.PARAMS_OFFLINE.max;
        let maxHistLen = params.maxHistLen || this.maxHistLen || NoteQuota.PARAMS_OFFLINE.maxHistLen;
        if (allowance !== this.allowance || max !== this.max || maxHistLen !== this.maxHistLen) {
            this.allowance = allowance;
            this.max = max;
            this.maxHistLen = maxHistLen;
            this.resetPoints();
            return true;
        }
        return false;
    }
    // FIXME: What type is this?
    getParams() {
        return {
            m: "nq",
            allowance: this.allowance,
            max: this.max,
            maxHistLen: this.maxHistLen
        };
    }
    resetPoints() {
        this.points = this.max;
        this.history = [];
        for (let i = 0; i < this.maxHistLen; i++)
            this.history.unshift(this.points);
        if (this.cb)
            this.cb(this.points);
    }
    tick() {
        // keep a brief history
        this.history.unshift(this.points);
        this.history.length = this.maxHistLen;
        // hook a brother up with some more quota
        if (this.points < this.max) {
            this.points = Math.min(this.points + this.allowance, this.max);
            // fire callback
            if (this.cb)
                this.cb(this.points);
        }
    }
    spend(needed) {
        // check whether aggressive limitation is needed
        let sum = 0;
        for (let i in this.history) {
            sum += this.history[i];
        }
        if (sum <= 0)
            needed *= this.allowance;
        // can they afford it?  spend
        if (this.points < needed) {
            return false;
        }
        else {
            this.points -= needed;
            if (this.cb)
                this.cb(this.points); // fire callback
            return true;
        }
    }
}
NoteQuota.PARAMS_LOBBY = { allowance: 200, max: 600 };
NoteQuota.PARAMS_NORMAL = { allowance: 400, max: 1200 };
NoteQuota.PARAMS_RIDICULOUS = { allowance: 600, max: 1800 };
NoteQuota.PARAMS_OFFLINE = { allowance: 8000, max: 24000, maxHistLen: 3 };


/***/ }),
/* 11 */
/***/ ((module) => {



module.exports = function () {
  throw new Error(
    'ws does not work in the browser. Browser clients must use the native ' +
      'WebSocket object'
  );
};


/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Bot__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var _MPPAPI_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7);
/* harmony import */ var ws__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(11);
/* harmony import */ var ws__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ws__WEBPACK_IMPORTED_MODULE_2__);



console.log(`tsbot starting...`);
if (typeof globalThis.WebSocket == 'undefined') {
    globalThis.WebSocket = ws__WEBPACK_IMPORTED_MODULE_2__;
}
if (typeof (globalThis.MPP) == 'undefined') {
    // node
    globalThis.MPP = {
        client: new _MPPAPI_api__WEBPACK_IMPORTED_MODULE_1__.Client("wss://www.multiplayerpiano.net:8080"),
        noteQuota: new _MPPAPI_api__WEBPACK_IMPORTED_MODULE_1__.NoteQuota(points => { return; }),
        press: () => { },
        release: () => { },
        pressSustain: () => { },
        releaseSustain: () => { },
        piano: {},
        Notification: {},
        chat: {}
    };
    MPP.client.start();
    MPP.client.setChannel('lobby');
    MPP.client.on('hi', msg => {
        MPP.client.sendArray([{ m: 'userset', set: { name: "TS Bot" } }]);
    });
}
else {
    // web
    MPP.noteQuota.points = Infinity;
}
globalThis.Bot = _Bot__WEBPACK_IMPORTED_MODULE_0__.default;
_Bot__WEBPACK_IMPORTED_MODULE_0__.default.start(MPP.client);

})();

/******/ })()
;