import { Participant } from "./MPPAPI/Client";
// import * as level from "level";

interface User extends Participant {
    [key: string]: any;
    groups: string[];
}

// node only
// if (typeof (globalThis as any).localStorage == 'undefined') {
//     let level = (require as any)('level');
//     let db = level("./bot.db");
//     (globalThis as any).localStorage = {
//         getItem: async (key: string) => {
//             return await db.get(key);
//         },
//         setItem: async (key: string, value: string) => {
//             return await db.put(key, value);
//         }
//     }
// }

class Database {
    static getUser(_id: string): User {
        let data: string = <string> localStorage.getItem("ts~"+_id);
        return <User> JSON.parse(<string> data);
    }

    static updateUser(_id: string, data: any) {
        let user: User = <User> this.getUser(_id);
        for (let key of Object.keys(data)) {
            user[key] = data[key];
        }
        localStorage.setItem("ts~"+_id, JSON.stringify(data));
    }

    static createUser(data: User) {
        let user: User = <User> data;
        user.groups = ["default"];
        localStorage.setItem("ts~"+user._id, JSON.stringify(data));
        return user;
    }
}

export {
    Database,
    User
}
