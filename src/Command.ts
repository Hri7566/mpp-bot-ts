import Bot from "./Bot";
import { RegistryKey } from "./RegistryKey";
import { RegistryObject } from "./RegistryObject";

class Command extends RegistryObject {
    accessors: string[];
    usage: string;
    desc: string;
    minimumArguments: number;
    permissionNode: string;
    hidden: boolean;
    func: Function;
    
    constructor (_id: RegistryKey, accessors: string[], usage: string, desc: string, func: Function, minargs: number, node: string, hidden: boolean) {
        super();
        this._id = "command." + _id;
        this.accessors = accessors;
        this.func = func;
        if (!this.func) this.func = () => {return `This command has no function.`};
        this.addPermission(node);
        this.hidden = hidden;

        this.desc = desc || "No description.";
        this.usage = usage || "No usage.";

        Bot.register(this._id, this);
    }

    addPermission(node: string) {
        this.permissionNode = node;
    }

    static getUsage(usage: string, prefixAccessor: string): string {
        return `${usage.split("$PREFIX").join(prefixAccessor)}`;
    }
}

class AdminCommand extends Command {
    constructor (_id: RegistryKey, accessors: string[], usage: string, desc: string, func: Function, minargs: number, node: string, hidden: boolean) {
        super(_id, accessors, usage, desc, func, minargs, `admin.${node}`, hidden);
    }

    addPermission(node: string) {
        this.permissionNode = "admincommand." + node;
    }
}

export {
    Command,
    AdminCommand
}
