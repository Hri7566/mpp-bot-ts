import Bot from "./Bot";
import { Client, MPPAPI, NoteQuota } from "./MPPAPI/api";
import * as WebSocket from "ws";

console.log(`tsbot starting...`);

declare var MPP: MPPAPI;
if (typeof (globalThis as any).WebSocket == 'undefined') {
    (globalThis as any).WebSocket = WebSocket;
}

if (typeof((globalThis as any).MPP) == 'undefined') {
    // node
    (globalThis as any).MPP = {
        client: new Client("wss://www.multiplayerpiano.net:8080"),
        noteQuota: new NoteQuota(points => {return;}),
        press: () => {},
        release: () => {},
        pressSustain: () => {},
        releaseSustain: () => {},
        piano: {},
        Notification: {},
        chat: {}
    }

    MPP.client.start();
    MPP.client.setChannel('lobby');
    MPP.client.on('hi', msg => {
        MPP.client.sendArray([{m:'userset', set:{name:"TS Bot"}}]);
    });
} else {
    // web
    MPP.noteQuota.points = Infinity;
}

(globalThis as any).Bot = Bot;

Bot.start(MPP.client);
