import { Client } from "./MPPAPI/api";
import * as $ from "jquery";
import { RegistryObject } from "./RegistryObject";
import { Permissions } from "./Permissions";
import * as _ from "lodash";
import { AdminCommand, Command } from "./Command";
import { PermissionGroup } from "./PermissionGroup";
import { Database, User } from "./Database";

interface CommandMessage {
    [key: string]: any;
}

class Prefix extends RegistryObject {
    _id: string;
    accessor: string;

    constructor (_id: string, acc: string) {
        super();
        this._id = `prefix.${_id}`;
        this.accessor = acc;

        Bot.register(this._id, this);
    }
}

export default class Bot {
    static client: Client;
    static registry: Map<string, any>;

    static Database: Database = Database;

    static start(client: Client): Bot {
        this.client = client;
        this.registry = new Map<string, any>();
        this.bindDefaultPermissionGroups();
        this.bindPrefixes();
        this.bindDefaultCommands();
        this.bindEventListeners();

        if (Database.getUser(<string> this.client.getOwnParticipant()._id) == null) {
            let user = Database.createUser(<User> this.client.getOwnParticipant());
            user.groups.push("owner");
            Database.updateUser(<string> this.client.getOwnParticipant()._id, user);
        }

        return this;
    }

    static bindDefaultPermissionGroups() {
        new PermissionGroup("default").addPermission("command.*");
        new PermissionGroup("admin").addPermission("admincommand.*");
        new PermissionGroup("special").addPermission("special.*");
        new PermissionGroup("owner").addPermission("*");
    }

    static bindDefaultCommands(): void {
        new Command("help", ["help", "cmds", "h"], `$PREFIXhelp (command)`, `List commands.`, (msg: any, bot: any) => {
            let out = "";
            if (!msg.args[1]) {
                out = "Commands: ";
                for (let i of this.registry.keys()) {
                    if (!i.startsWith('command')) continue;
                    let cmd: Command = <Command> this.registry.get(i);
                    if (!cmd.hidden) {
                        let hasPermission = false;
                        for (let g of <string[]> msg.user.groups) {
                            if (PermissionGroup.hasPermission(g, cmd.permissionNode)) {
                                hasPermission = true;
                                break;
                            }
                        }

                        if (hasPermission) {
                            out += ` ${msg.usedPrefix.accessor}${cmd.accessors[0]} | `;
                        }
                    }
                }
                out = out.substr(0, out.length - 2).trim();
            } else {
                out = `There is no help for '${msg.argcat}'.`;
                for (let i of this.registry) {
                    if (!i[0].startsWith('command')) continue;
                    let cmd: Command = <Command> i[1];

                    let cont = false;
                    for (let a of cmd.accessors) {
                        if (msg.argcat == a) {
                            cont = true;
                            break;
                        }
                    }
                    if (!cont) continue;

                    out = `${cmd.desc} Usage: ${Command.getUsage(cmd.usage, msg.usedPrefix.accessor)}`;
                }
            }
            return out;
        }, 0, "command.help", false);

        new Command("about", ["about"], `$PREFIXabout`, `About the bot.`, (msg: any, bot: any) => {
            return `Written in TypeScript and compiled with webpack by Hri7566.`;
        }, 0, "command.about", false);

        // admin commands

        new AdminCommand("permission", ["permission", "perm", "permissions", "perms", "luckperms", "lp"], `$PREFIXpermission [group|user] (create) [groupId [set|check] [permission] [value]|userId [addgroup] [groupId]]`, `Change permissions of groups or add users to groups`, (msg: any, bot: any) => {
            if (msg.args[1] == 'group') {
                if (!msg.args[2]) return `Usage: group (create) [groupId] [set|check] [permission] [value]`;
                if (msg.args[2] == 'create') {
                    if (!msg.args[3]) return `Provide a group name.`;
                    new PermissionGroup(msg.args[3]);
                    return `Created group '${msg.args[3]}'.`;
                } else {
                    let group: PermissionGroup = this.registry.get(`permissiongroup.${msg.args[2]}`);
                    if (!group) return `No group named '${msg.args[2]}' was found.`;
                    if (!msg.args[3]) return `Usage: group [groupId] [set|check] [permission] [value]`;
                    if (msg.args[3] == "set") {
                        if (!msg.args[4] && (!msg.args[5] || (msg.args[5] !== 'true' && msg.args[5] !== 'false'))) return `Usage: group [groupId] [set] [permission] [true|false]`;
                        if (msg.args[5] == "true") {
                            group.addPermission(msg.args[4]);
                        } else if (msg.args[5] == "false") {
                            group.removePermission(msg.args[4]);
                        } else {
                            return `Usage: group [groupId] [set] [permission] [true|false]`;
                        }
                    } else if (msg.args[3] == "check") {
                        if (!msg.args[4]) return `Usage: group [groupId] [check] [permission]`;
                        let has = group.permissions.indexOf(msg.args[4]) !== -1;
                        return has ? `Group ${group._id} has ${msg.args[4]}` : `Group ${group._id} does not have ${msg.args[4]}`;
                    } else {
                        return `Usage: group [groupId] [set|check] [permission] [value]`;
                    }
                }
            } else if (msg.args[1] == 'user') {
                if (!msg.args[2] || !msg.args[3]) return `Usage: user [userId] [addgroup] [groupId]`;
                let user = Database.getUser(msg.args[2]);
                if (!user) return `No user with the _id '${msg.args[2]}' was found.`;
                if (msg.args[3] == "addgroup") {
                    if (!msg.args[4]) return `Usage: user [userId] addgroup [groupId]`;
                    let group: PermissionGroup = this.registry.get(`permissiongroup.${msg.args[4]}`);
                    if (!group) return `No group named '${msg.args[4]}' was found.`;
                    user.groups.push(msg.args[4]);
                    Database.updateUser(<string> user._id, user);
                    return `Added group '${msg.args[4]}' to user '${user._id}'.`;
                } else {
                    return `Usage: user [userId] addgroup [groupId]`;
                }
            }
        }, 1, `admincommand.permission`, false);

        new Command("permissiongroup", ["permissiongroup", "pg", "rank", "r"], `$PREFIXpermissiongroup`, `Check what groups you are in.`, (msg: CommandMessage, bot: any) => {
            return `Groups: ${msg.user.groups.join(", ")}`;
        }, 0, `command.permissiongroup`, false);
    }

    static bindEventListeners(): void {
        if (typeof (globalThis as any).require !== "undefined") {
            this.client.on('hi', (msg: any) => {
                this.client.sendArray([({
                    m: "userset",
                    set: {
                        name: "7566",
                        color: "#8d3f50"
                    }
                } as any)]);
            });
        }

        this.client.on('a', (msg: CommandMessage) => {
            msg.args = msg.a.split(' ');

            for (let i of this.registry.keys()) {
                if (!i.startsWith('prefix')) continue;
                
                let prefix: Prefix = <Prefix> this.registry.get(i);
                if (msg.args[0].startsWith(prefix.accessor)) {
                    msg.usedPrefix = prefix;
                    break;
                }
            }

            if (!msg.usedPrefix) return;

            msg.cmd = msg.args[0].substring(msg.usedPrefix.accessor.length);
            msg.argcat = msg.a.substring(msg.args[0].length).trim();

            msg.user = Database.getUser(msg.p._id);

            
            if (!msg.user) msg.user = Database.createUser(<User> msg);
            
            this.handleCommandMessage(msg);
        });
    }
    
    static handleCommandMessage(msg: CommandMessage): void {
        for (let i of this.registry) {
            if (!i[0].startsWith('command')) continue;
            let cmd: Command = <Command> this.registry.get(i[0]);
            
            let canContinue = false;
            for (let a of cmd.accessors) {
                if (msg.cmd == a) {
                    canContinue = true;
                    break;
                }
            }
            if (canContinue == false) continue;
            
            
            let hasPermission = false;
            for (let g of <string[]> msg.user.groups) {
                if (PermissionGroup.hasPermission(g, cmd.permissionNode)) {
                    hasPermission = true;
                    break;
                }
            }
            
            if (hasPermission == false) {
                this.sendChat(`You do not have permission to use this command.`);
                break;
            }

            if (msg.args.length - 1 < cmd.minimumArguments) {
                this.sendChat(`Not enough arguments. Usage: ${Command.getUsage(cmd.usage, msg.usedPrefix.accessor)}`);
                break;
            }

            try {
                let out = cmd.func(msg, this);
                if (typeof(out) !== 'string') break;
                if (out !== null && out !== '') {
                    this.sendChat(out);
                }
            } catch (err) {
                this.sendChat(`An error has occurred.`);
                console.error(err);
            }
        }
    }

    static bindPrefixes(): void {
        new Prefix("slash", "/");
        new Prefix("exclamation", "!");
    }

    static register(node: string, obj: RegistryObject): void {
        this.registry.set(node, obj);
    }

    static sendChat(str: string): void {
        this.client.sendArray([{m:"a", message:`\u034f${str}`}]);
    }

    static [Symbol.toPrimitive](): any {
        return "🤡";
    }

    static [Symbol.toStringTag](): any {
        return `🤡`;
    }
}
