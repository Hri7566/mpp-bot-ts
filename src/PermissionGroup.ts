import { RegistryKey } from "./RegistryKey";
import { RegistryObject } from "./RegistryObject";
import { User } from "./Database";
import Bot from "./Bot";
import { Permissions } from "./Permissions";

class PermissionGroup extends RegistryObject {
    permissions: string[];

    constructor (_id: RegistryKey) {
        super();
        this._id = "permissiongroup." + _id;
        this.permissions = [];
        Bot.register(this._id, this);
    }

    addPermission(key: string): number {
        return this.permissions.push(key);
    }

    removePermission(key: string): any {
        return this.permissions.splice(this.permissions.indexOf(key));
    }

    static hasPermission(g: string, key: string) {
        let group: PermissionGroup = Bot.registry.get("permissiongroup." + g);
        let out = false;
        
        for (let i of group.permissions) {
            if (Permissions.hasPermission(i, key)) {
                out = true;
                break;
            }
        }

        return out;
    }

    static getGroups(user: User): PermissionGroup[] {
        let out: PermissionGroup[] = [];
        for (let g of user.groups) {
            if (Bot.registry.has("permissiongroup." + g)) {
                out.push(Bot.registry.get("permissiongroup." + g));
            }
        }

        if (out.length == 0) {
            out = [Bot.registry.get("permissiongroup.default")];
        }

        return out;
    }
}

export {
    PermissionGroup
}
