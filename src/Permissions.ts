class Permissions {
    /**
     * Checks if a permission string is included within another.
     * @param perm1 The permission that needs to be checked
     * @param perm2 The permission to check against
     */
    public static hasPermission(perm1: string, perm2: string): boolean {
        let p1: string[] = perm1.split('.');
        let p2: string[] = perm2.split('.');

        let good: boolean = false;

        for (let i in p1) {
            if (p1[i] == p2[i] || p1[i] == '*') {
                good = true;
                continue;
            } else {
                good = false;
                break;
            }
        }

        return good;
    }
}

export {
    Permissions
}