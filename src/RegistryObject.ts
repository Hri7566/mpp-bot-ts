import { RegistryKey } from "./RegistryKey";

abstract class RegistryObject {
    _id: RegistryKey;
}

export {
    RegistryObject
}
