import { Client } from "./Client";
import { NoteQuota } from "./NoteQuota";

interface MPPAPI {
    client: Client;
    piano: any;
    Notification: any;
    chat: any;
    noteQuota: NoteQuota;
    press: Function;
    pressSustain: Function;
    release: Function;
    releaseSustain: Function;
}

export {
    MPPAPI,
    Client,
    NoteQuota
}
